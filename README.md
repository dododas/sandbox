### Welcome!

This project documents how to create a GitLab project. It also demonstrates math rendering in Markdown documents hosted on Gitlab, such as this one.

---

#### 1. Create project
- Go to https://gitlab.com and create a new project called `sandbox`

#### 2. Clone project to local machine
- If necessary, first set generate an SSH key pair and add the public key to GitLab. This allows the local machine to push changes to the project. Follow [these instructions](https://docs.gitlab.com/ce/ssh/README.html). 
- Clone repository on local machine:

    ```shell
    git clone git@gitlab.com:dododas/sandbox.git
    ```

#### 3. Add content
- Create a `README` (this file)

    ```shell
    cd sandbox
    touch README.md
    ```

- Edit `README.md` with a text editor [(click here](https://gitlab.com/dododas/sandbox/raw/master/README.md) to view the plain text version of this document). 
- Gitlab supports math rendering! (<a href="https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md#math" target="_blank">see details here</a>). 
    - Here is an inline expression: $`e^{i \theta} = \cos \theta + i \sin \theta`$ (produced by ``$`e^{i \theta} = \cos \theta + i \sin \theta\`$``) 
    - And here is a display mode expression:

    ```math
    e^z = \lim_{n \to \infty} \left(1 + \frac{z}{n} \right)^n
    ```

#### 4. Push updates
- When ready, commit!
    
    ```shell
    git add README.md
    git commit -m "first commit: add README"
    git push -u origin master
    ```

And we're done... :thumbsup: 

